﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mission_1
{
    public partial class CnForm : Form
    {
        public CnForm()
        {
            InitializeComponent();
        }

        private void ok_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public Dictionary<int, string> ProviderData //свойство - пара методов
        {
            set
            {
                prov_comboBox.DataSource = value.ToArray();
                prov_comboBox.DisplayMember = "value";
            }
        }

        public int ProvId
        {
            get
            {
                return ((KeyValuePair<int, string>)prov_comboBox.SelectedItem).Key;
            }
            set
            {
                int index = 0;
                foreach (KeyValuePair<int, string> item in prov_comboBox.Items)
                {
                    if (item.Key == value) break;
                    index++;
                }
                prov_comboBox.SelectedIndex = index;
            }
        }
    }
}
