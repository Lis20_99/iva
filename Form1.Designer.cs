﻿namespace Mission_1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.Main_tabControl = new System.Windows.Forms.TabControl();
            this.abonent_tab = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_FIO_Abo = new System.Windows.Forms.TextBox();
            this.delete_button = new System.Windows.Forms.Button();
            this.edit_button = new System.Windows.Forms.Button();
            this.button_add = new System.Windows.Forms.Button();
            this.abonent_dgv = new System.Windows.Forms.DataGridView();
            this.contact_tab = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_phone_Con = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.add_button = new System.Windows.Forms.Button();
            this.contact_dgv = new System.Windows.Forms.DataGridView();
            this.provider_tab = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.provider_dgv = new System.Windows.Forms.DataGridView();
            this.resoult_tab = new System.Windows.Forms.TabPage();
            this.delete_telbutton = new System.Windows.Forms.Button();
            this.adit_telbutton = new System.Windows.Forms.Button();
            this.add_telbutton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label_phone = new System.Windows.Forms.Label();
            this.label_FIO = new System.Windows.Forms.Label();
            this.textBox_phone_Tel = new System.Windows.Forms.TextBox();
            this.textBox_FIO_Tel = new System.Windows.Forms.TextBox();
            this.resoult_dgv = new System.Windows.Forms.DataGridView();
            this.Main_tabControl.SuspendLayout();
            this.abonent_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.abonent_dgv)).BeginInit();
            this.contact_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contact_dgv)).BeginInit();
            this.provider_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.provider_dgv)).BeginInit();
            this.resoult_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resoult_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            // 
            // Main_tabControl
            // 
            this.Main_tabControl.Controls.Add(this.abonent_tab);
            this.Main_tabControl.Controls.Add(this.contact_tab);
            this.Main_tabControl.Controls.Add(this.provider_tab);
            this.Main_tabControl.Controls.Add(this.resoult_tab);
            this.Main_tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Main_tabControl.Location = new System.Drawing.Point(0, 0);
            this.Main_tabControl.Name = "Main_tabControl";
            this.Main_tabControl.SelectedIndex = 0;
            this.Main_tabControl.Size = new System.Drawing.Size(784, 501);
            this.Main_tabControl.TabIndex = 1;
            // 
            // abonent_tab
            // 
            this.abonent_tab.Controls.Add(this.label1);
            this.abonent_tab.Controls.Add(this.label4);
            this.abonent_tab.Controls.Add(this.textBox_FIO_Abo);
            this.abonent_tab.Controls.Add(this.delete_button);
            this.abonent_tab.Controls.Add(this.edit_button);
            this.abonent_tab.Controls.Add(this.button_add);
            this.abonent_tab.Controls.Add(this.abonent_dgv);
            this.abonent_tab.Location = new System.Drawing.Point(4, 22);
            this.abonent_tab.Name = "abonent_tab";
            this.abonent_tab.Padding = new System.Windows.Forms.Padding(3);
            this.abonent_tab.Size = new System.Drawing.Size(776, 475);
            this.abonent_tab.TabIndex = 0;
            this.abonent_tab.Text = "Абоненты";
            this.abonent_tab.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(552, 386);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Поиск";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(431, 407);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "По ФИО";
            // 
            // textBox_FIO_Abo
            // 
            this.textBox_FIO_Abo.Location = new System.Drawing.Point(434, 423);
            this.textBox_FIO_Abo.Name = "textBox_FIO_Abo";
            this.textBox_FIO_Abo.Size = new System.Drawing.Size(159, 20);
            this.textBox_FIO_Abo.TabIndex = 5;
            this.textBox_FIO_Abo.TextChanged += new System.EventHandler(this.textBox_FIO_Abo_TextChanged);
            // 
            // delete_button
            // 
            this.delete_button.Location = new System.Drawing.Point(215, 389);
            this.delete_button.Name = "delete_button";
            this.delete_button.Size = new System.Drawing.Size(100, 25);
            this.delete_button.TabIndex = 3;
            this.delete_button.Text = "Удалить";
            this.delete_button.UseVisualStyleBackColor = true;
            this.delete_button.Click += new System.EventHandler(this.delete_button_Click);
            // 
            // edit_button
            // 
            this.edit_button.Location = new System.Drawing.Point(109, 389);
            this.edit_button.Name = "edit_button";
            this.edit_button.Size = new System.Drawing.Size(100, 25);
            this.edit_button.TabIndex = 2;
            this.edit_button.Text = "Редактировать";
            this.edit_button.UseVisualStyleBackColor = true;
            this.edit_button.Click += new System.EventHandler(this.edit_button_Click);
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(3, 389);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(100, 25);
            this.button_add.TabIndex = 1;
            this.button_add.Text = "Добавить";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // abonent_dgv
            // 
            this.abonent_dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.abonent_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.abonent_dgv.Location = new System.Drawing.Point(3, 3);
            this.abonent_dgv.MultiSelect = false;
            this.abonent_dgv.Name = "abonent_dgv";
            this.abonent_dgv.Size = new System.Drawing.Size(770, 380);
            this.abonent_dgv.TabIndex = 0;
            // 
            // contact_tab
            // 
            this.contact_tab.Controls.Add(this.label5);
            this.contact_tab.Controls.Add(this.label6);
            this.contact_tab.Controls.Add(this.textBox_phone_Con);
            this.contact_tab.Controls.Add(this.button4);
            this.contact_tab.Controls.Add(this.button3);
            this.contact_tab.Controls.Add(this.add_button);
            this.contact_tab.Controls.Add(this.contact_dgv);
            this.contact_tab.Location = new System.Drawing.Point(4, 22);
            this.contact_tab.Name = "contact_tab";
            this.contact_tab.Padding = new System.Windows.Forms.Padding(3);
            this.contact_tab.Size = new System.Drawing.Size(776, 475);
            this.contact_tab.TabIndex = 1;
            this.contact_tab.Text = "Контакты";
            this.contact_tab.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(552, 386);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Поиск";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(599, 407);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "По номеру телефона";
            // 
            // textBox_phone_Con
            // 
            this.textBox_phone_Con.Location = new System.Drawing.Point(602, 423);
            this.textBox_phone_Con.Name = "textBox_phone_Con";
            this.textBox_phone_Con.Size = new System.Drawing.Size(171, 20);
            this.textBox_phone_Con.TabIndex = 5;
            this.textBox_phone_Con.TextChanged += new System.EventHandler(this.textBox_phone_Con_TextChanged);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(215, 389);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 25);
            this.button4.TabIndex = 4;
            this.button4.Text = "Удалить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(109, 389);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 25);
            this.button3.TabIndex = 3;
            this.button3.Text = "Редактировать";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // add_button
            // 
            this.add_button.Location = new System.Drawing.Point(3, 389);
            this.add_button.Name = "add_button";
            this.add_button.Size = new System.Drawing.Size(100, 25);
            this.add_button.TabIndex = 2;
            this.add_button.Text = "Добавить";
            this.add_button.UseVisualStyleBackColor = true;
            this.add_button.Click += new System.EventHandler(this.add_button_Click);
            // 
            // contact_dgv
            // 
            this.contact_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.contact_dgv.Location = new System.Drawing.Point(3, 3);
            this.contact_dgv.MultiSelect = false;
            this.contact_dgv.Name = "contact_dgv";
            this.contact_dgv.Size = new System.Drawing.Size(770, 380);
            this.contact_dgv.TabIndex = 0;
            // 
            // provider_tab
            // 
            this.provider_tab.Controls.Add(this.button2);
            this.provider_tab.Controls.Add(this.button5);
            this.provider_tab.Controls.Add(this.button6);
            this.provider_tab.Controls.Add(this.provider_dgv);
            this.provider_tab.Location = new System.Drawing.Point(4, 22);
            this.provider_tab.Name = "provider_tab";
            this.provider_tab.Size = new System.Drawing.Size(776, 475);
            this.provider_tab.TabIndex = 2;
            this.provider_tab.Text = "Провайдеры";
            this.provider_tab.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(215, 389);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 25);
            this.button2.TabIndex = 7;
            this.button2.Text = "Удалить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(109, 389);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(100, 25);
            this.button5.TabIndex = 6;
            this.button5.Text = "Редактировать";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(3, 389);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(100, 25);
            this.button6.TabIndex = 5;
            this.button6.Text = "Добавить";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // provider_dgv
            // 
            this.provider_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.provider_dgv.Location = new System.Drawing.Point(3, 3);
            this.provider_dgv.MultiSelect = false;
            this.provider_dgv.Name = "provider_dgv";
            this.provider_dgv.Size = new System.Drawing.Size(770, 380);
            this.provider_dgv.TabIndex = 0;
            // 
            // resoult_tab
            // 
            this.resoult_tab.Controls.Add(this.delete_telbutton);
            this.resoult_tab.Controls.Add(this.adit_telbutton);
            this.resoult_tab.Controls.Add(this.add_telbutton);
            this.resoult_tab.Controls.Add(this.label3);
            this.resoult_tab.Controls.Add(this.label_phone);
            this.resoult_tab.Controls.Add(this.label_FIO);
            this.resoult_tab.Controls.Add(this.textBox_phone_Tel);
            this.resoult_tab.Controls.Add(this.textBox_FIO_Tel);
            this.resoult_tab.Controls.Add(this.resoult_dgv);
            this.resoult_tab.Location = new System.Drawing.Point(4, 22);
            this.resoult_tab.Name = "resoult_tab";
            this.resoult_tab.Size = new System.Drawing.Size(776, 475);
            this.resoult_tab.TabIndex = 3;
            this.resoult_tab.Text = "Телефонный Справочник";
            this.resoult_tab.UseVisualStyleBackColor = true;
            // 
            // delete_telbutton
            // 
            this.delete_telbutton.Location = new System.Drawing.Point(215, 389);
            this.delete_telbutton.Name = "delete_telbutton";
            this.delete_telbutton.Size = new System.Drawing.Size(100, 25);
            this.delete_telbutton.TabIndex = 7;
            this.delete_telbutton.Text = "Удалить";
            this.delete_telbutton.UseVisualStyleBackColor = true;
            this.delete_telbutton.Click += new System.EventHandler(this.delete_telbutton_Click);
            // 
            // adit_telbutton
            // 
            this.adit_telbutton.Location = new System.Drawing.Point(109, 389);
            this.adit_telbutton.Name = "adit_telbutton";
            this.adit_telbutton.Size = new System.Drawing.Size(100, 25);
            this.adit_telbutton.TabIndex = 6;
            this.adit_telbutton.Text = "Редактировать";
            this.adit_telbutton.UseVisualStyleBackColor = true;
            this.adit_telbutton.Click += new System.EventHandler(this.adit_telbutton_Click);
            // 
            // add_telbutton
            // 
            this.add_telbutton.Location = new System.Drawing.Point(3, 389);
            this.add_telbutton.Name = "add_telbutton";
            this.add_telbutton.Size = new System.Drawing.Size(100, 25);
            this.add_telbutton.TabIndex = 5;
            this.add_telbutton.Text = "Добавить";
            this.add_telbutton.UseVisualStyleBackColor = true;
            this.add_telbutton.Click += new System.EventHandler(this.add_telbutton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(552, 386);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Поиск";
            // 
            // label_phone
            // 
            this.label_phone.AutoSize = true;
            this.label_phone.Location = new System.Drawing.Point(599, 407);
            this.label_phone.Name = "label_phone";
            this.label_phone.Size = new System.Drawing.Size(113, 13);
            this.label_phone.TabIndex = 2;
            this.label_phone.Text = "По номеру телефона";
            // 
            // label_FIO
            // 
            this.label_FIO.AutoSize = true;
            this.label_FIO.Location = new System.Drawing.Point(431, 407);
            this.label_FIO.Name = "label_FIO";
            this.label_FIO.Size = new System.Drawing.Size(51, 13);
            this.label_FIO.TabIndex = 2;
            this.label_FIO.Text = "По ФИО";
            // 
            // textBox_phone_Tel
            // 
            this.textBox_phone_Tel.Location = new System.Drawing.Point(602, 423);
            this.textBox_phone_Tel.Name = "textBox_phone_Tel";
            this.textBox_phone_Tel.Size = new System.Drawing.Size(171, 20);
            this.textBox_phone_Tel.TabIndex = 1;
            this.textBox_phone_Tel.TextChanged += new System.EventHandler(this.textBox_phone_TextChanged);
            // 
            // textBox_FIO_Tel
            // 
            this.textBox_FIO_Tel.Location = new System.Drawing.Point(434, 423);
            this.textBox_FIO_Tel.Name = "textBox_FIO_Tel";
            this.textBox_FIO_Tel.Size = new System.Drawing.Size(159, 20);
            this.textBox_FIO_Tel.TabIndex = 1;
            this.textBox_FIO_Tel.TextChanged += new System.EventHandler(this.textBox_FIO_TextChanged);
            // 
            // resoult_dgv
            // 
            this.resoult_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resoult_dgv.Location = new System.Drawing.Point(3, 3);
            this.resoult_dgv.Name = "resoult_dgv";
            this.resoult_dgv.Size = new System.Drawing.Size(770, 380);
            this.resoult_dgv.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 501);
            this.Controls.Add(this.Main_tabControl);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Main_tabControl.ResumeLayout(false);
            this.abonent_tab.ResumeLayout(false);
            this.abonent_tab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.abonent_dgv)).EndInit();
            this.contact_tab.ResumeLayout(false);
            this.contact_tab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contact_dgv)).EndInit();
            this.provider_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.provider_dgv)).EndInit();
            this.resoult_tab.ResumeLayout(false);
            this.resoult_tab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resoult_dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl Main_tabControl;
        private System.Windows.Forms.TabPage abonent_tab;
        private System.Windows.Forms.TabPage contact_tab;
        private System.Windows.Forms.TabPage provider_tab;
        private System.Windows.Forms.TabPage resoult_tab;
        private System.Windows.Forms.DataGridView abonent_dgv;
        private System.Windows.Forms.DataGridView contact_dgv;
        private System.Windows.Forms.DataGridView provider_dgv;
        private System.Windows.Forms.DataGridView resoult_dgv;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_phone;
        private System.Windows.Forms.Label label_FIO;
        private System.Windows.Forms.TextBox textBox_phone_Tel;
        private System.Windows.Forms.TextBox textBox_FIO_Tel;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button edit_button;
        private System.Windows.Forms.Button delete_button;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button add_button;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button delete_telbutton;
        private System.Windows.Forms.Button adit_telbutton;
        private System.Windows.Forms.Button add_telbutton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_FIO_Abo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_phone_Con;
    }
}

