﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mission_1
{
    public partial class TelForm : Form
    {
        public TelForm()
        {
            InitializeComponent();
        }

        private void ok_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public Dictionary<int, string> FIOData //свойство - пара методов
        {
            set
            {
                fio_comboBox.DataSource = value.ToArray();
                fio_comboBox.DisplayMember = "value";
            }
        }

        public Dictionary<int, string> PhoneData //свойство - пара методов
        {
            set
            {
                phone_comboBox.DataSource = value.ToArray();
                phone_comboBox.DisplayMember = "value";
            }
        }

        public int FIOId
        {
            get
            {
                return ((KeyValuePair<int, string>)fio_comboBox.SelectedItem).Key;
            }
        }

        public int PhoneId
        {
            get
            {
                return ((KeyValuePair<int, string>)phone_comboBox.SelectedItem).Key;
            }
        }
    }
}
