﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Mission_1
{

    public partial class Form1 : Form
    {
        //private const String ConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\Для учёбы\\Для учебы\\6-й семестр\\Новая папка\\Управление данными\\Проги\\Mission_1\\Database1.mdf;Integrated Security=True;Connect Timeout=30";
        private string ConnectionString = global::Mission_1.Properties.Settings.Default.Database1ConnectionString;
        public Form1()
        {
            InitializeComponent();
        }

        /*private void button1_Click(object sender, EventArgs e)
        {
            string query = "SELECT surname FROM Lapshin_abonent";
            SqlDataAdapter aOrder = new SqlDataAdapter(query, ConnectionString);
            DataTable table = new DataTable();
            aOrder.Fill(table);
            string res = "";
            foreach(DataRow row in table.Rows)
            {
                res += row["surname"].ToString() + "  ";
            }

            MessageBox.Show(res, "Просто учебная кнопка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

        }
        */
        // Короче чтобы не писать каждый раз 
        void updateAbonentGDV()
        {
            var request = "SELECT *FROM Lapshin_abonent";
            if (textBox_FIO_Abo.Text != "")
            {
                request += " WHERE (Lapshin_abonent.name+' '+Lapshin_abonent.surname+' '+Lapshin_abonent.patronymic) LIKE '%" + textBox_FIO_Abo.Text + "%'";
            }
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var abonentTable = new DataTable();
            adapter.Fill(abonentTable);

            abonent_dgv.DataSource = abonentTable;
            abonent_dgv.Columns["Id"].Visible = false;
            abonent_dgv.Columns["name"].HeaderText = "Имя";
            abonent_dgv.Columns["surname"].HeaderText = "Фамилия";
            abonent_dgv.Columns["patronymic"].HeaderText = "Отчество";
            abonent_dgv.Columns["address"].HeaderText = "Адрес";
            abonent_dgv.Columns["birthday"].HeaderText = "Дата рождения";
            abonent_dgv.Columns["comment"].HeaderText = "Коментарий";
        }

        void updateContactGDV()
        {
            var request = "SELECT *FROM Lapshin_contact JOIN Lapshin_provider ON Lapshin_provider.Id = Lapshin_contact.provider_id";
            if (textBox_phone_Con.Text != "")
            {
                request += " WHERE Lapshin_contact.phone LIKE '%" + textBox_phone_Con.Text + "%'";
            }
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var contactTable = new DataTable();
            adapter.Fill(contactTable);
            contact_dgv.DataSource = contactTable;
            contact_dgv.Columns["Id"].Visible = false;
            contact_dgv.Columns["Id1"].Visible = false;
            contact_dgv.Columns["provider_id"].Visible = false;
            contact_dgv.Columns["score"].Visible = false;
            contact_dgv.Columns["phone"].HeaderText = "Номер телефона";
            contact_dgv.Columns["type"].HeaderText = "Тпи телефрона";
            contact_dgv.Columns["name"].HeaderText = "Название провайдера";
            // Название провайдера удалять или нет?
        }

        void updateProviderGDV()
        {
            var request = "SELECT *FROM Lapshin_provider";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var providerTable = new DataTable();
            adapter.Fill(providerTable);
            provider_dgv.DataSource = providerTable;
            provider_dgv.Columns["Id"].Visible = false;
            provider_dgv.Columns["name"].HeaderText = "Имя провайдера";
            provider_dgv.Columns["score"].HeaderText = "Рейтинг провайдера";
        }

        void updateTelspravGDV()
        {
            var request = "SELECT * FROM Lapshin_abonent ";
            request += "JOIN Lapshin_abonent_has_contact ";
            request += "ON Lapshin_abonent.Id = Lapshin_abonent_has_contact.abonent_id ";
            request += "JOIN Lapshin_contact ";
            request += "ON Lapshin_contact.Id=Lapshin_abonent_has_contact.contact_id ";
            request += "LEFT JOIN Lapshin_provider ";
            request += "ON Lapshin_provider.Id = Lapshin_contact.provider_id";
            /*if (textBox_phone_Tel.Text != "")
            {
                request += " WHERE Lapshin_contact.phone LIKE '%" + textBox_phone_Tel.Text + "%'";
            }

            if (textBox_FIO_Tel.Text != "")
            {
                request += " WHERE (Lapshin_abonent.name+' '+Lapshin_abonent.surname+' '+Lapshin_abonent.patronymic) LIKE '%" + textBox_FIO_Tel.Text + "%'";
            }*/

            //тест номер 4
            List<string> test = new List<string>();
            if (textBox_phone_Tel.Text != "")
            {
                test.Add("Lapshin_contact.phone LIKE + '%" + textBox_phone_Tel.Text + "%'");
            }
            if (textBox_FIO_Tel.Text != "")
            {
                test.Add("Lapshin_abonent.name+' '+Lapshin_abonent.surname+' '+Lapshin_abonent.patronymic LIKE + '%" + textBox_FIO_Tel.Text + "%'");
            }
            if (test.Capacity != 0)
            {
                request += " WHERE ";
                for (int i = 0; i < test.Count; i++)
                {
                    if (i == test.Count - 1) request += test[i];
                    else request += test[i] + " AND ";
                }
            }

            var adapter = new SqlDataAdapter(request, ConnectionString);
            var TelTable = new DataTable();
            adapter.Fill(TelTable);
            resoult_dgv.DataSource = TelTable;
            resoult_dgv.Columns["Id"].Visible = false;
            resoult_dgv.Columns["address"].Visible = false;
            resoult_dgv.Columns["birthday"].Visible = false;
            resoult_dgv.Columns["comment"].Visible = false;
            resoult_dgv.Columns["contact_id"].Visible = false;
            resoult_dgv.Columns["abonent_id"].Visible = false;
            resoult_dgv.Columns["Id1"].Visible = false;
            resoult_dgv.Columns["Id2"].Visible = false;
            resoult_dgv.Columns["type"].Visible = false;
            resoult_dgv.Columns["provider_id"].Visible = false;
            resoult_dgv.Columns["score"].Visible = false;
            resoult_dgv.Columns["name"].HeaderText = "Имя";
            resoult_dgv.Columns["surname"].HeaderText = "Фамилия";
            resoult_dgv.Columns["patronymic"].HeaderText = "Отчество";
            resoult_dgv.Columns["phone"].HeaderText = "Номер телефона";
            resoult_dgv.Columns["name1"].HeaderText = "Название провайдера";
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            updateAbonentGDV();
            updateContactGDV();
            updateProviderGDV();
            updateTelspravGDV();
        }

        private void textBox_phone_TextChanged(object sender, EventArgs e)
        {
            updateTelspravGDV();
        }

        private void textBox_FIO_TextChanged(object sender, EventArgs e)
        {
            updateTelspravGDV();
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            var form = new AbForm();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var surname = form.surname_textBox.Text;
                var name = form.name_textBox.Text;
                var patronymic = form.patronymic_textBox.Text;
                var comment = form.comment_textBox.Text;
                var address = form.address_textBox.Text;

                var birthday = form.birthday_textBox.Text;

                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Lapshin_abonent (surname, name, patronymic, comment, address, birthday) " +
                    "VALUES ('" + surname + "', '" + name + "', '" + patronymic + "', '" + comment + "', '" + address + "', '" + birthday + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateAbonentGDV();
            }
        }

        private void edit_button_Click(object sender, EventArgs e)
        {
            var row = abonent_dgv.SelectedRows.Count > 0 ? abonent_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new AbForm();
            form.surname_textBox.Text = row.Cells["surname"].Value.ToString();
            form.name_textBox.Text = row.Cells["name"].Value.ToString();
            form.patronymic_textBox.Text = row.Cells["patronymic"].Value.ToString();
            form.address_textBox.Text = row.Cells["address"].Value.ToString();
            form.comment_textBox.Text = row.Cells["comment"].Value.ToString();
            form.birthday_textBox.Text = row.Cells["birthday"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var surname = form.surname_textBox.Text;
                var name = form.name_textBox.Text;
                var patronymic = form.patronymic_textBox.Text;
                var comment = form.comment_textBox.Text;
                var address = form.address_textBox.Text;
                var birthday = form.birthday_textBox.Text;
                var id = row.Cells["Id"].Value.ToString();

                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Lapshin_abonent SET surname = '" + surname + "', name = '" + name + "', " +
                    "patronymic = '" + patronymic + "', address = '" + address + "', comment = '" + comment + "', birthday = '" + birthday + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateAbonentGDV();
            }
        }

        private void delete_button_Click(object sender, EventArgs e)
        {
            var row = abonent_dgv.SelectedRows.Count > 0 ? abonent_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Lapshin_abonent WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updateAbonentGDV();
        }

        private void add_button_Click(object sender, EventArgs e)
        {
            var form = new CnForm();
            {
                var request = "SELECT * FROM Lapshin_provider";
                var adapter = new SqlDataAdapter(request, ConnectionString);
                var providerTable = new DataTable();
                adapter.Fill(providerTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in providerTable.Rows)
                {
                    dict.Add((int)row["Id"], row["name"].ToString());
                }
                form.ProviderData = dict;
            }
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.phone_textBox.Text;
                var type = form.type_textBox.Text;
                var prov_id = form.ProvId;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Lapshin_contact (phone, type, provider_id) VALUES ('" + phone + "', '" + type + "', '" + prov_id.ToString() + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateContactGDV();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var row = contact_dgv.SelectedRows.Count > 0 ? contact_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new CnForm();
            form.phone_textBox.Text = row.Cells["phone"].Value.ToString();
            form.type_textBox.Text = row.Cells["type"].Value.ToString();
            {
                var request = "SELECT * FROM Lapshin_provider";
                var adapter = new SqlDataAdapter(request, ConnectionString);
                var providerTable = new DataTable();
                adapter.Fill(providerTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow dbRow in providerTable.Rows)
                {
                    dict.Add((int)dbRow["Id"], dbRow["name"].ToString());
                }
                form.ProviderData = dict;
            }
            form.ProvId = (int)row.Cells["provider_id"].Value;
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.phone_textBox.Text;
                var type = form.type_textBox.Text;
                var id = row.Cells["Id"].Value.ToString();
                var prov_id = form.ProvId;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Lapshin_contact SET phone = '" + phone + "', type = '" + type + "', " +
                    "provider_id = '" + prov_id.ToString() + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateContactGDV();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var row = contact_dgv.SelectedRows.Count > 0 ? contact_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Lapshin_contact WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updateContactGDV();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var form = new PrForm();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.name_textBox.Text;
                var score = form.score_textBox.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Lapshin_provider (name, score) VALUES ('" + name + "', '" + score + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateProviderGDV();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var row = provider_dgv.SelectedRows.Count > 0 ? provider_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new PrForm();
            form.name_textBox.Text = row.Cells["name"].Value.ToString();
            form.score_textBox.Text = row.Cells["score"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.name_textBox.Text;
                var score = form.score_textBox.Text;
                var id = row.Cells["Id"].Value.ToString();
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Lapshin_provider SET name = '" + name + "', score = '" + score + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateProviderGDV();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var row = provider_dgv.SelectedRows.Count > 0 ? provider_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Lapshin_provider WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updateProviderGDV();
            updateAbonentGDV();
            updateContactGDV();
            updateTelspravGDV();
        }

        private void add_telbutton_Click(object sender, EventArgs e)
        {
            var form = new TelForm();
            {
                var getReq = "SELECT Id, surname, name, patronymic FROM Lapshin_abonent";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }
                form.FIOData = dict;
            }
            {
                var getReq = "SELECT Id, phone FROM Lapshin_contact";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }
                form.PhoneData = dict;
            }
            if (form.ShowDialog() == DialogResult.OK)
            {
                var conn = new SqlConnection(ConnectionString);
                conn.Open();
                var request = "INSERT INTO Lapshin_abonent_has_contact" + "(abonent_id, contact_id)" + " VALUES " + "('" + form.FIOId + "', '" + form.PhoneId + "')";
                var com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();
                conn.Close();
                updateTelspravGDV();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            /*var row1 = resoult_dgv.SelectedRows.Count > 0 ? resoult_dgv.SelectedRows[0] : null;
            if (row1 == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new TelForm();
            {
                var getReq = "SELECT Id, surname, name, patronymic FROM Lapshin_abonent";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Table = new DataTable();
                Adapter.Fill(Table);
                foreach (DataRow row in Table.Rows)
                {
                    string setStr = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setStr);
                }
                form.FIOData = dict;
            }
            {
                var getReq = "SELECT Id, phone FROM Lapshin_contact";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Table = new DataTable();
                Adapter.Fill(Table);

                foreach (DataRow row in Table.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }
                form.PhoneData = dict;
            }
            DataGridViewSelectedRowCollection Row = resoult_dgv.SelectedRows;

            var mas = resoult_dgv.SelectedRows;
            var Condidat = mas[0].Cells["Id"].FormattedValue.ToString();
            var Condidat2 = mas[0].Cells["Id1"].FormattedValue.ToString();

            var conn = new SqlConnection(ConnectionString);
            conn.Open();
            string str = "";
            var req = "SELECT surname FROM Lapshin_abonent WHERE Id = " + Condidat + "";
            var com = new SqlCommand(req, conn);
            var last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT name FROM Lapshin_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT patronymic FROM Lapshin_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last;
            form.fio_comboBox.Text = str;

            req = "SELECT phone FROM Lapshin_contact WHERE Id = " + Condidat2 + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            form.phone_comboBox.Text = last.ToString();
            conn.Close();
            if (form.ShowDialog() == DialogResult.OK)
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "UPDATE Lapshin_abonent_has_contact " + " SET contact_id='" + form.PhoneId +
                    "', " + "abonent_id ='" + form.FIOId + "' WHERE contact_id=" + Condidat2 +
                    " AND abonent_id=" + Condidat;
                com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();
                conn.Close();
                updateTelspravGDV();
            }*/
        }

        private void button8_Click(object sender, EventArgs e)
        {
            /*var row = resoult_dgv.SelectedRows.Count > 0 ? resoult_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var num1 = row.Cells["Id"].Value.ToString();
            var num2 = row.Cells["Id1"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Lapshin_abonent_has_contact WHERE contact_id = '" + num2 +
                "' AND abonent_id='" + num1 + "'";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updateTelspravGDV();*/
        }

        private void textBox_FIO_Abo_TextChanged(object sender, EventArgs e)
        {
            updateAbonentGDV();
        }

        private void textBox_phone_Con_TextChanged(object sender, EventArgs e)
        {
            updateContactGDV();
        }

        private void adit_telbutton_Click(object sender, EventArgs e)
        {
            var row1 = resoult_dgv.SelectedRows.Count > 0 ? resoult_dgv.SelectedRows[0] : null;
            if (row1 == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new TelForm();
            {
                var getReq = "SELECT Id, surname, name, patronymic FROM Lapshin_abonent";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Table = new DataTable();
                Adapter.Fill(Table);
                foreach (DataRow row in Table.Rows)
                {
                    string setStr = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setStr);
                }
                form.FIOData = dict;
            }
            {
                var getReq = "SELECT Id, phone FROM Lapshin_contact";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Table = new DataTable();
                Adapter.Fill(Table);

                foreach (DataRow row in Table.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }
                form.PhoneData = dict;
            }
            DataGridViewSelectedRowCollection Row = resoult_dgv.SelectedRows;

            var mas = resoult_dgv.SelectedRows;
            var Condidat = mas[0].Cells["Id"].FormattedValue.ToString();
            var Condidat2 = mas[0].Cells["Id1"].FormattedValue.ToString();

            var conn = new SqlConnection(ConnectionString);
            conn.Open();
            string str = "";
            var req = "SELECT surname FROM Lapshin_abonent WHERE Id = " + Condidat + "";
            var com = new SqlCommand(req, conn);
            var last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT name FROM Lapshin_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT patronymic FROM Lapshin_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last;
            form.fio_comboBox.Text = str;

            req = "SELECT phone FROM Lapshin_contact WHERE Id = " + Condidat2 + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            form.phone_comboBox.Text = last.ToString();
            conn.Close();
            if (form.ShowDialog() == DialogResult.OK)
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "UPDATE Lapshin_abonent_has_contact " + " SET contact_id='" + form.PhoneId +
                    "', " + "abonent_id ='" + form.FIOId + "' WHERE contact_id=" + Condidat2 +
                    " AND abonent_id=" + Condidat;
                com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();
                conn.Close();
                updateTelspravGDV();
            }
        }

        private void delete_telbutton_Click(object sender, EventArgs e)
        {
            var row = resoult_dgv.SelectedRows.Count > 0 ? resoult_dgv.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var num1 = row.Cells["Id"].Value.ToString();
            var num2 = row.Cells["Id1"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Lapshin_abonent_has_contact WHERE contact_id = '" + num2 +
                "' AND abonent_id='" + num1 + "'";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updateTelspravGDV();
        }
    }
}
